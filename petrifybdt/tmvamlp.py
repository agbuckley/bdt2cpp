#! /usr/bin/env python
# -*- python -*-

"""
Translate a TMVA MLP classifier to Keras and then to ONNX

Scikit-learn cannot be used for this because the exported ONNX file can't be validated by reading it in via TMVA SOFIE:
- `skl2onnx` parses the "classes" field as `string` or `int32`
  (https://github.com/onnx/sklearn-onnx/blob/2bd2d8f59160d6acdbdfc5ed12f3f819941e1f78/skl2onnx/operator_converters/multilayer_perceptron.py#L122)
  but TMVA can only handle `int64` and `float` (https://root.cern/doc/v632/RModelParser__ONNX_8cxx_source.html#l00374).
  Therefore it is required to edit `multilayer_perceptron.py` from `skl2onnx` by hand to amend this.
- Something in a scikit-learn MLP is parsed as operator `ArgMax`
  (https://github.com/onnx/sklearn-onnx/blob/2bd2d8f59160d6acdbdfc5ed12f3f819941e1f78/skl2onnx/operator_converters/multilayer_perceptron.py#L192)
  and TMVA does not support `ArgMax`
  (https://github.com/root-project/root/issues/10360, https://root.cern/doc/v632/RModelParser__ONNX_8cxx_source.html#l00089 and 
  https://root.cern/doc/master/RModelParser__ONNX_8cxx_source.html#l00245).
  That is a shame as `ArgMax` is part of ONNX since operator set 1 (https://github.com/onnx/onnx/blob/rel-1.9.1/docs/Operators.md).
  (Choosing a concrete `opset` version does not solve the issue but merely has TMVA complain about being unable to fuse something
  *earlier* in the parsing, starting with version 6. Tested were 1, 2, 3, 5, 6, 8, 10, 14.)
"""

import petrifybdt_common as pc

ap = pc.get_args_parser()
ap.add_argument("--test", dest="TEST", action="store_true", help="Test the MLP on a sample dataset.")
args = ap.parse_args()

print("Initialising.")

import os
import xml.etree.ElementTree as ET
import pandas as pd
import numpy as np

os.environ["TF_USE_LEGACY_KERAS"] = "1" # use legacy keras (pip install tf_keras), see https://github.com/tensorflow/tensorflow/issues/64515#issuecomment-2036195525

import tensorflow as tf
import tf_keras
import tf2onnx # alternative "keras2onnx" is not under active development

class ROOTNormalizeTransform(tf_keras.layers.Layer):
    """ Linear transformation layer for normalization
        Inspired by https://github.com/root-project/root/blob/de5fab1649ca92acf463c3d267daff2e0803c8b4/tmva/tmva/src/VariableNormalizeTransform.cxx#L145
    """
    def __init__(self, ranges, dtype=np.float32):
        super().__init__()
        for r in ranges:
            assert len(r)==2, f"Items should be (minimum, maximum) but given was '{r}'"
        
        ranges = np.array(ranges, dtype=dtype)
        self.offsets = tf.constant(ranges[:,0], dtype=dtype)
        diffs = ranges[:,1]-ranges[:,0]
        scales = 1./diffs
        self.double_scales = tf.constant(scales * 2., dtype=dtype)
        self.ones = tf.ones(len(ranges), dtype=dtype)

    def call(self, inputs):
        return tf.math.multiply( (inputs - self.offsets), self.double_scales) - self.ones

def parse_options(nn):
    options = {}

    for token in str(nn.GetOptions()).split(":"):
        token = token[1:] # remove leading "~"
        name, value = token.split("=")
        options[name] = value
    
    return options

def get_option(dic, xvar, name, cast_type):
    if xvar.get("name")==name:
        value = cast_type(xvar.text)

        # special cases
        if name=="HiddenLayers":
            value = [int(x) for x in value.split(",")] # TMVA actually uses one more neuron than claimed

        # assign
        dic[name] = value

# Get all items with @name_item in category @name_category from root @xroot
def get_category(xroot, name_category, name_item):
    return xroot.find(name_category).findall(name_item)

# ==========================================================================
# Process XML
# ==========================================================================
print("Processing XML.")
# have to parse XML because TMVA MLP API doesn't expose hidden layers, weights, etc.

# general params
param_dict = {}
xroot = ET.parse(args.FILE).getroot()
for xvar in get_category(xroot, "Options", "Option"):
    get_option(param_dict, xvar, "NeuronType", str)
    get_option(param_dict, xvar, "EstimatorType", str)
    get_option(param_dict, xvar, "HiddenLayers", str)
    get_option(param_dict, xvar, "LearningRate", float)

# classes
classes = [xvar.get("Name") for xvar in get_category(xroot, "Classes", "Class")]
classes = np.array(range(len(classes)), dtype=np.dtype("int64"))
n_class = len(classes)
loss = "binary_crossentropy"
if n_class>2:
    loss = "sparse_categorical_crossentropy"

# variables
variables = []
for xvar in get_category(xroot, "Variables", "Variable"):
    var_name = xvar.get("Label")
    var_type = pc.parse_type(xvar.get("Type"))
    assert var_type==np.float32, f"ONNX can only handle 'np.float32' at this point, but given was '{var_type}'"
    variables.append( (var_name, var_type) )
dtype = var_type

# value ranges
ranges = []
transformations = get_category(xroot, "Transformations", "Transform")
assert len(transformations)==1, f"Can currently only handle 1 transformation but number of given transformations was {len(transformations)}"
range_class = transformations[0].findall("Class")[-1] # TMVA uses only last set of ranges
xml_ranges = range_class.findall("Ranges")
assert len(xml_ranges)==1, f"Can only handle 1 set of ranges but number of given sets of ranges were {len(xml_ranges)}"
for xvar in xml_ranges[0]:
    minimum = xvar.get("Min")
    maximum = xvar.get("Max")
    ranges.append( np.array([minimum, maximum], dtype=dtype) )

# dummy data
data = {}
for var_name, var_type in variables: # initialise with dummy data of correct type and correct length for number of classes
    arr = list(range(n_class))*1000
    data[var_name] = np.array(arr, dtype=var_type)#np.empty(n_class, dtype=var_type)
    #data[var_name] = np.array(range(n_class), dtype=var_type)#np.empty(n_class, dtype=var_type)
classes = np.array(list(range(n_class))*1000, dtype=np.dtype("int64"))
data["y"] = classes
df = pd.DataFrame(data=data)

if args.VERBOSE:
    print(df)
X_train = df.drop("y", axis=1)

# ==========================================================================
# Apply to Keras classifier
# ==========================================================================
print("Applying to Keras classifier.")

# Create the model
models = {}
for name in ["nominal", "noNorm"]:
    models[name] = tf_keras.models.Sequential(name=args.NAME)

nn_layers = param_dict["HiddenLayers"][:1]+param_dict["HiddenLayers"] # first layer is duplicate

# add linear transformation layer for normalization
models["nominal"].add(ROOTNormalizeTransform(ranges, dtype=dtype))

# have to set values before initialisation because model.weights is read-only
layout = xroot.find("Weights").find("Layout")
for layer_id, tmva_layer in enumerate(layout.findall("Layer")):
    # TMVA lists final (empty) neuron as extra layer => skip this layer
    # use "continue" instead of "break" to crash if there's larger layer multiplicity mismatch
    if layer_id==len(nn_layers):
        continue

    neurons = []
    biasses = []

    for neuron_id, neuron in enumerate(tmva_layer.findall("Neuron")):
        neuron_str = neuron.text.strip() # sanitise string

        coeffs = []
        coeffs = np.empty((len(neuron_str.split(" "))), dtype=dtype)
        for coeff_id, coeff in enumerate(neuron_str.split(" ")):
            coeffs[coeff_id] = coeff
        n = tf.stack(coeffs)

        # last TMVA neuron is actually bias: https://root.cern/doc/master/MethodANNBase_8cxx_source.html#l00372
        target = biasses if neuron_id==nn_layers[layer_id] else neurons

        target.append(n)

    init_kernel = tf_keras.initializers.constant(tf.stack(neurons))
    init_bias   = tf_keras.initializers.constant(tf.stack(biasses))

    units = -1
    activation = param_dict["NeuronType"]
    if layer_id+1>=len(nn_layers): # last layer gives category
        if loss=="binary_crossentropy":
            units = 1
        elif loss=="sparse_categorical_crossentropy":
            units = n_class

        # last output neuron can take different function: https://root.cern/doc/master/MethodANNBase_8cxx_source.html#l00305
        if param_dict["EstimatorType"]=="CE":
            activation = "sigmoid"
        elif param_dict["EstimatorType"]=="MSE":
            activation = "linear"
        else:
            raise Exception("Unknown estimator type "+param_dict["EstimatorType"])
    else:
        units = nn_layers[layer_id+1]

    tf_layer = tf_keras.layers.Dense(units, dtype=dtype, activation=activation, trainable=False, kernel_initializer=init_kernel, bias_initializer=init_bias)
    for model in models.values():
        model.add(tf_layer)


# Configure the model and start training
sgd = tf_keras.optimizers.SGD(learning_rate=param_dict["LearningRate"])
for model in models.values():
    model.compile(loss=loss, optimizer=sgd, metrics=["accuracy"])
    model.fit(X_train.to_numpy(), classes, epochs=1, verbose=args.VERBOSE)

if args.VERBOSE:
    models["nominal"].summary()

if args.TEST:
    line = 0
    dataset = X_train.to_numpy()[line]
    result = models["nominal"].predict(tf.expand_dims(dataset, axis=0), verbose=args.VERBOSE)
    print("Test result: ", result)

# ==========================================================================
# Export to ONNX
# ==========================================================================
for model_name, model in models.items():
    output_name = args.NAME
    if model_name!="nominal":
        output_name += "_"+model_name
    output_name += ".onnx"
    print(f"\nWriting model to {output_name}")
    if model_name=="noNorm":
        print("This model does not normalize the inputs, e.g. for usage with ROOT. You need to manually scale the inputs according to")
        print("\t- The procedure detailed at https://github.com/root-project/root/blob/de5fab1649ca92acf463c3d267daff2e0803c8b4/tmva/tmva/src/VariableNormalizeTransform.cxx#L145")
        print("\t- Using the values given at Transformations/Transform/Class/Ranges from the XML file")
    print("\n")

    onx, external_tensor_storage = tf2onnx.convert.from_keras(model)
    with open(output_name, "wb") as f:
        f.write(onx.SerializeToString())
