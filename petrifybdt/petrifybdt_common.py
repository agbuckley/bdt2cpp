#! /usr/bin/env python
# -*- python -*-

"""
Common tools for the convertors
"""

def get_args_parser():
    import argparse
    ap = argparse.ArgumentParser(__doc__)
    ap.add_argument("FILE", nargs="?", help="File containing the model")
    ap.add_argument("-n", "--name", dest="NAME", default="decision", help="name to use for the output function and source file [default=%(default)s]")
    ap.add_argument("-v", "--verbose", dest="VERBOSE", action="store_true", help="Print debug output.")

    return ap

# Parse a variable type from a string
def parse_type(type_string):
    import numpy as np
    if type_string=="D" or type_string=="F": # cannot differentiate between float and double
        return np.dtype("f")

    raise TypeError(f"Type '{type_string}' is not understood.")

# Manually parse the XML file to obtain the property names and types
def get_properties(input_file, property_name, item_name, input_list=None, verbose=False):
    from array import array

    if verbose:
        print(f"PROPERTY NAME: {property_name}")

    props = []
    if input_list:
        for p in input_list.split(","):
            if ":" in p:
                prop_name, prop_type = p.split(":")
            else:
                prop_name = p
                prop_type = "f"
            prop_type_arr = array(parse_type(prop_type), [0])
            props.append( (prop_name, prop_type_arr) )
    else:
        import xml.etree.ElementTree as ET
        xtree = ET.parse(input_file)
        xvars = xtree.getroot().find(property_name)
        for xvar in xvars.findall(item_name): # strip plural "s"
            prop_name = xvar.get("Label")
            prop_type = xvar.get("Type")
            prop_type_arr = array(parse_type(prop_type), [0])
            props.append( (prop_name, prop_type_arr) )

    if verbose:
        print(f"Found {property_name}: {props}")

    return props

# Manually parse the XML file to obtain the variable names and types
def get_variables(args):
    return get_properties(args.FILE, "Variables", "Variable", args.VARIABLES, args.VERBOSE)

def add_variables_to_reader(reader, args):
    vars = get_variables(args)

    for var_name, var_type in vars:
        try:
            reader.AddVariable(var_name, var_type)
        except TypeError as te:
            print(f"\nERROR: Type '{var_type}' is not recognised by TMVA.")
            raise te